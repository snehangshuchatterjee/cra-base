import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './tooltip.scss';

class Tooltip extends Component {
    render() {
        const { placeholderText, bodyText } = this.props;
        return (
            <div class="tooltip">{placeholderText}
                <span class="tooltiptext">{bodyText}</span>
            </div>
        );
    }
}


Tooltip.propTypes = {
    placeholderText: PropTypes.string,
    bodyText: PropTypes.string
};

Tooltip.defaultProps = {
    placeholderText: "hello",
    bodyText: "world"
};


export default Tooltip;
