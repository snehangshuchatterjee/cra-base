import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Textbox extends Component {
    render() {
        const { style, placeHolderText, onChange } = this.props;
        return (
            <div>
                <input type="text"
                    className="basic_textbox"
                    style={style}
                    placeHolder={placeHolderText}
                    onChange={onChange}
                />
            </div>
        );
    }
}


Textbox.propTypes = {
    style: PropTypes.object,
    placeHolderText: PropTypes.string
};

Textbox.defaultProps = {
    style: {},
    placeHolderText: "Enter Text Here"
}


export default Textbox;
